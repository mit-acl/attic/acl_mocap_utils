%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compare VICON and OptiTrack solutions
%
% This script compares the pose output of VICON and OptiTrack for the
% specified rigid body. It reads 'body_vicon' and 'body_optitrack' from a
% ROS bag. Using the first second of samples, frames are registered. We are
% assuming that {vicon dots} == {optitrack dots} (i.e., when the rigid body
% objects were made in VICON Tracker and OptiTrack Motive software, the
% robot was in the same exact spot).
%
% Parker Lusk
% 19 July 2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear, clc;

bagfile = '~/Documents/bags/mocap/acl_optitrack_2019-07-20-17-34-12.bag';
% bagfile = '~/Documents/bags/mocap/acl_optitrack_2019-07-20-17-36-19.bag';
[vicon, optitrack] = readACLBag('HX02', bagfile);

% Register frames
ts = 0; te = 1; % time period of no movement with samples from both
T_VO = findOptitrackwrtVicon(vicon, optitrack, ts, te)

% Put optitrack in VICON frame
optitrack = registerFrame(optitrack, T_VO);

% Plot trajectory
figure(1), clf;
hold on; grid on;
plot(vicon.position(1,:), vicon.position(2,:),'LineWidth',1);
plot(optitrack.position(1,:), optitrack.position(2,:),'LineWidth',1);
legend('VICON','OptiTrack'); title('X-Y Trajectory');
minmx = min(vicon.position(1,:)); maxmx = max(vicon.position(1,:));
minmy = min(vicon.position(2,:)); maxmy = max(vicon.position(2,:));
minvx = min(optitrack.position(1,:)); maxvx = max(optitrack.position(1,:));
minvy = min(optitrack.position(2,:)); maxvy = max(optitrack.position(2,:));
xaxis([min(minmx,minvx) max(maxmx,maxvx)]);
yaxis([min(minmy,minvy) max(maxmy,maxvy)]);
xlabel('X [m]'); ylabel('Y [m]'); axis equal

% XYZ traces
figure(3), clf;
for i = 1:3
    subplot(3,1,i); hold on; grid on;
    plot(vicon.t, vicon.position(i,:), 'k--','LineWidth',1);
    plot(optitrack.t, optitrack.position(i,:),'LineWidth',1);
    xlabel('Time [s]'); ylabel([char(87+i) ' [m]']);
end
subplot(311); legend('VICON','OptiTrack'); title('Position');

function pose = registerFrame(pose, T)
p = pose.position;
q = pose.quaternion;

N = length(p);

% put pose data into a bunch of SE(3) transforms
data = [quat2rotm(q') reshape(p,3,1,[]);...
        zeros(1,3,N) ones(1,1,N)];

% slow, but who cares
for i = 1:N, data(:,:,i) = T*data(:,:,i); end

% extract
p = reshape(data(1:3,4,:),3,[]);
q = rotm2quat(data(1:3,1:3,:))';

pose.position = p;
pose.quaternion = q;
end

function T_VO = findOptitrackwrtVicon(vicon, optitrack, ts, te)
if (te-ts <= 0)
    T_VO = eye(4);
    return;
end

% find the samples corresponding from the time period
vNs = find(vicon.t>=ts,1); vNe = find(vicon.t>=te,1);
if isempty(vNe), vNe = length(vicon.t); end
oNs = find(optitrack.t>=ts,1); oNe = find(optitrack.t>=te,1);
if isempty(oNe), oNe = length(optitrack.t); end

% R3 average
p_VVD = mean(vicon.position(:,vNs:vNe),2);
p_OOD = mean(optitrack.position(:,vNs:vNe),2);

% SO(3) average
R_VVD = rotAvg(quat2rotm(vicon.quaternion(:,vNs:vNe)'));
R_OOD = rotAvg(quat2rotm(optitrack.quaternion(:,oNs:oNe)'));

% Create SE(3) transforms
T_VVD = [R_VVD p_VVD; 0 0 0 1];
T_OOD = [R_OOD p_OOD; 0 0 0 1];

% Find transform
T_VO = T_VVD * inv(T_OOD);
end

function R = rotAvg(Rs)
N = length(Rs);
X = 1/N*sum(Rs,3);
[U,~,V] = svd(X);
R = U*diag([1,1,det(U*V')])*V';
end