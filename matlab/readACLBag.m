function [vicon, optitrack] = readACLBag(veh, bagpath)
%READBAG Extracts messages into matrices without using custom msg defn

t = @(topic) ['/' veh '/' topic];

bag = rosbag(bagpath);

% Get mocap ground truth messages -- PoseStamped
bagsel = select(bag, 'Topic', t('body_vicon'));
msgs = readMessages(bagsel,'DataFormat','struct'); poseStampedMsg = [msgs{:}];
tsec = header([poseStampedMsg.Header]);
[pos, quat] = PoseMessage([poseStampedMsg.Pose]);
vicon = struct('t',tsec,'position',pos,'quaternion',quat);

% Get mocap ground truth messages -- PoseStamped
bagsel = select(bag, 'Topic', t('body_optitrack'));
msgs = readMessages(bagsel,'DataFormat','struct'); poseStampedMsg = [msgs{:}];
tsec = header([poseStampedMsg.Header]);
[pos, quat] = PoseMessage([poseStampedMsg.Pose]);
optitrack = struct('t',tsec,'position',pos,'quaternion',quat);

% it seems like some optitrack times are wrong? Maybe there was a network
% timesync during?
I = find(optitrack.t - optitrack.t(1) < 0);
optitrack.t(I) = [];
optitrack.position(:,I) = [];
optitrack.quaternion(:,I) = [];

% time sync
mint = min([min(vicon.t), min(optitrack.t)]);
vicon.t = vicon.t - mint;
optitrack.t = optitrack.t - mint;

end

function t = header(headerMsg)
stamp = [headerMsg.Stamp];
sec = [stamp.Sec];
nsec = [stamp.Nsec];
t = double(sec) + double(nsec)*1e-9;
end

function [pos, quat] = PoseMessage(poseMsg)
pos = Vector3Message([poseMsg.Position]);
quat = QuaternionMessage([poseMsg.Orientation]);
end

function vec = Vector3Message(vectorMsg)
X = [vectorMsg.X]; Y = [vectorMsg.Y]; Z = [vectorMsg.Z];
vec = [X' Y' Z']';
end

function q = QuaternionMessage(quatMsg)
W = [quatMsg.W]; X = [quatMsg.X]; Y = [quatMsg.Y]; Z = [quatMsg.Z];
q = [W' X' Y' Z']';
end