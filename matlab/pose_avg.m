%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pose averaging (least-squares soln)
%
% This script shows that SLERP and Frobenius-norm-based rotation matrix
% averaging gives the same answer.
%
% Parker Lusk
% 20 March 2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear, clc;

% vicon dots w.r.t vicon ; optitrack dots w.r.t optitrack
raw_VVD = csvread('~/.ros/vicondots_wrt_vicon.csv');
raw_OOD = csvread('~/.ros/optitrackdots_wrt_optitrack.csv');
N = length(raw_VVD);

% body w.r.t vicon dots
T_VDB = eye(4);
% body w.r.t optitrack dots
T_ODB = eye(4);
% T_ODB(1:3,1:3) = eul2rotm([pi/2, 0, 0]); % bebop2 -- pi/2 yaw

% A place to put everything
pbar_VO = zeros(N,3);
qbar_VO = zeros(N,4);
Rbar_VO = zeros(3,3,N);

%% Cumulative Moving Average and SLERP
% initialize the average pos, quat (cum moving avg)
p_VO = zeros(1,3);
q_VO = [1 0 0 0];

for i = 1:N
    R_VVD = quat2rotm(raw_VVD(i,4:7));
    t_VVD = raw_VVD(i,1:3);
    R_OOD = quat2rotm(raw_OOD(i,4:7));
    t_OOD = raw_OOD(i,1:3);
    
    % Package up into SE3 matrices for ease
    T_VVD = eye(4); T_VVD(1:3,1:3) = R_VVD; T_VVD(1:3,4) = t_VVD;
    T_OOD = eye(4); T_OOD(1:3,1:3) = R_OOD; T_OOD(1:3,4) = t_OOD;

    % new measurement: optitrack frame w.r.t vicon frame
    Tbar_VO = (T_VVD*T_VDB) * inv(T_OOD*T_ODB);
    
    % break out rotation (quat) and translation measurement updates
    pbar_VO(i,:) = Tbar_VO(1:3,4);
    Rbar_VO(:,:,i) = Tbar_VO(1:3,1:3);
    qbar_VO(i,:) = rotm2quat(Rbar_VO(:,:,i));

    % cumulative moving average
    n = i-1;
    p_VO = 1/(n+1) * (n*p_VO + pbar_VO(i,:));

    % SLERP for quats
    q_VO = quatinterp(q_VO, qbar_VO(i,:), 1/(n+1));
end

p_VO
q_VO

% compare quat avg with slerp
qmean_VO = meanrot(quaternion(qbar_VO))

%% batch process
% rot avg
X = 1/N*sum(Rbar_VO,3);
[U,S,V] = svd(X);
R_VO = U*diag([1,1,det(U*V')])*V';
qRavg_VO = rotm2quat(R_VO)
% trans avg
tavg_VO = 1/N*sum(pbar_VO,1)