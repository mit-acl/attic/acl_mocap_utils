ACL Mocap Utils
===============

Motion capture tools for comibning VICON and OptiTrack. This allows robots to have ground truth across the entire highbay space.

## Background

The highbay has two separate motion capture systems from the two different labs, ACL (west side) and FAST (east side). We have a VICON system, while FAST has an OptiTrack system. We would like to be able to fly across the entire highbay while using ground truth from the camera systems. For a single robot, we have the following geometric coordinate frame relationships:

```
                    {World}
                   /   |   \
            T_WV  /    |    \  T_WO
                 /     |     \
                /      |      \
         {VICON}       |       {OptiTrack}
            |          |            |
            |          |            |
      T_VVD |          |            | T_OOD
            |     T_WB |            |
    {vicon_dots}       |     {optitrack_dots}
                \      |      /
                 \     |     /
            T_VDB \    |    / T_ODB
                   \   |   /
                    \  |  /
                     {body}
```

**Note**: The notation for transformations is loosely based on [Drake](https://drake.mit.edu/doxygen_cxx/group__multibody__notation__basics.html). For example, `T_WB` should be read "body frame w.r.t world frame" and is the transformation needed to convert measurements in the body frame into the world frame. Said another way, `T_WB` is the pose of the body in the world frame.

- **World**: This is the local inertial frame of the highbay.
- **VICON**: The origin of the VICON camera system. Although not required, it is most likely the case that it is identified with the world frame (i.e., `T_WV = I`).
- **OptiTrack**: The origin of the OptiTrack camera system.
- **vicon_dots**: Motion capture systems do not give the pose of the robot body. Instead, they stream the pose of the rigid body, based on the placement of the markers (dots) when the rigid body is created in the motion capture software. Care must be taken so that the robot body x-axis is aligned with the vicon x-axis when the rigid body is first created. If this is not the case, `T_VDB` can be set to account for this offset. Otherwise, `T_VDB` is assumed to be identity.
- **optitrack_dots**: Same as **vicon_dots**.
- **body**: This is the body frame of the robot. The desired pose is `T_WB`, the body w.r.t the world.

### Note on OptiTrack Origin

The OptiTrack Motive 2 software creates its coordinate system as North-Up-East, whereas the VICON system uses East-North-Up. The `optitrack-motive-2-client` takes care of transforming this NUE system into ENU, so by the time data hits the ROS network, the OptiTrack coordinate system is already ENU (same orientation as VICON). In this README, we will refer to this ENU OptiTrack coordinate system as simply the OptiTrack origin.

## Calibration

The `calibrator` node finds the relative transform between the VICON and OptiTrack origins. To calibrate the `T_WO` transform:

1. Create a VICON rigid body. Remember to align body and VICON x-axis. If not aligned, then the `calibrator/body_wrt_vicon_dots` rosparam can be used to specify misalignment.
2. Create a OptiTrack rigid body. Remember to align body and OptiTrack x-axis. If not aligned, then the `calibrator/body_wrt_vicon_dots` rosparam can be used to specify misalignment.
3. Run `roslaunch acl_mocap_utils calibrate.launch`
4. Place the robot in the transition region. Ensure that the robot is visible from both camera systems (either via `rostopic echo ...` or the `rviz` display).
5. To sample and calibrate, run `rosservice call /calibrator/sample`.
6. Repeat the above two steps for different robot locations (in the transition region). This will improve the accuracy of the average transformation.
7. Each time the `/calibrator/sample` service is called, the calibration is saved in a YAML file which can be specified via the `calibration_file` rosparam.

## Mocap Fusion


## Rigid Body to Body Misalignment

As noted in the **Background** section, motion capture systems only return the pose of the rigid body markers, **not** the pose of the robot. For this reason, it is important to align the body axes with the camera origin axes when first creating the rigid body. If this cannot be done, or a more exact transform is required, the non-identity transforms can be passed to the calibrator node via rosparams. The format for these is shown in the following example:

Given a robot whose mocap rigid body (e.g., optitrack_dots) was aligned with the mocap y-axis, the following rosparam would be set *before* running the `acl_mocap_utils` node:

```
# The format is the same as a tf static_broadcaster: "x y z Y P R"
$ rosparam set /calibrator/body_wrt_optitrack_dots "0 0 0 1.5708 0 0"
```
