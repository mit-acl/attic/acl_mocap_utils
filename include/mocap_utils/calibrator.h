/**
 * @file calibrator.h
 * @brief For calibrating transform between VICON and OptiTrack
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 28 Feb 2019
 */

#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>

#include <ros/ros.h>

#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_broadcaster.h>

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>

#include <std_srvs/Trigger.h>

#include <acl_msgs/ViconState.h>

#include "mocap_utils/utils.h"
#include "mocap_utils/tf2_helpers.h"
#include "mocap_utils/rigid_body.h"

namespace acl {
namespace mocap_utils {

  class Calibrator
  {
  public:
    Calibrator(const ros::NodeHandle nh);
    ~Calibrator() = default;
    
  private:
    ros::NodeHandle nh_;
    ros::Timer tim_tf_broadcast_;
    ros::ServiceServer srv_sample_;
    ros::Subscriber sub_optitrack_, sub_vicon_;
    ros::Publisher pub_vicon_body_, pub_optitrack_body_, pub_err_;
    tf2_ros::TransformBroadcaster tf_br_;

    geometry_msgs::TransformStamped T_WV_; ///< vicon frame w.r.t world
    geometry_msgs::TransformStamped T_WO_; ///< optitrack frame w.r.t world

    tf2::Transform T_VDB_; ///< body w.r.t vicon dots
    tf2::Transform T_ODB_; ///< body w.r.t optitrack dots

    // These are the latest published poses and are used for calculating error.
    geometry_msgs::PoseStamped msg_T_VB_; ///< body w.r.t vicon
    geometry_msgs::PoseStamped msg_T_OB_; ///< body w.r.t optitrack

    bool capture_ = false; ///< should we be filling a buffer with transforms?

    int nrCaptures_ = 0; ///< num captures that have been performed
    int nrSamples_; ///< num transform samples to use each capture
    std::vector<tf2::Transform> buf_T_VVD_, buf_T_OOD_; ///< transform buffers

    std::string calibFile_; ///< path of where to save calibration file

    // Callbacks to receive VICON and OptiTrack data
    void viconCb(const acl_msgs::ViconStatePtr& msg);
    void optitrackCb(const acl_msgs::ViconStatePtr& msg);

    void timerCb(const ros::TimerEvent& e);

    /**
     * @brief      Calculate the relative transform between VICON and OptiTrack
     */
    void calibrate();

    /**
     * @brief      Handle ROS service request to sample current mocap poses.
     *             Also runs the calibration routine.
     *
     *
     * @param      req   request content (empty)
     * @param      res   response (true)
     *
     * @return     True if response handled
     */
    bool sampleSrvCb(std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res);
  };

}
}
