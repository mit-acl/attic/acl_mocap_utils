/**
 * @file fuser.h
 * @brief Fuse VICON and OptiTrack for the highbay
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 2 March 2019
 */

#pragma once

#include <iostream>
#include <string>
#include <unordered_set>
#include <regex>
#include <map>
#include <tuple>

#include <ros/ros.h>
#include <ros/master.h>

#include <tf2_ros/static_transform_broadcaster.h>

#include <geometry_msgs/TransformStamped.h>

#include "mocap_utils/utils.h"
#include "mocap_utils/rigid_body.h"

namespace acl {
namespace mocap_utils {

  class Fuser
  {
  public:
    Fuser(const ros::NodeHandle nh);
    ~Fuser() = default;
    
  private:
    ros::NodeHandle nh_;
    ros::Timer tim_main_;

    double loopPeriod_; ///< Period of fusion loop.
                        ///< Should be ~1/2 of slowest mocap stream.

    double alpha_; ///< LPF constant for pose LPF, 0 ≤ α ≤ 1.
                   ///< α == 0 => no filtering.

    std::string calibFile_; ///< location of calibration file with T_WO
    tf2_ros::StaticTransformBroadcaster staticBr_; ///< for broadcasting T_WO
    geometry_msgs::TransformStamped T_WO_; ///< optitrack frame w.r.t world

    std::map<std::string, RigidBodyPtr> robots_; ///< key: robot name

    void timerCb(const ros::TimerEvent& e);

  };

}
}
