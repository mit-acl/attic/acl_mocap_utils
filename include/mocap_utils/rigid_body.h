/**
 * @file rigid_body.h
 * @brief A rigid body object can subscribe to VICON and/or OptiTrack
 *        streams and rebroadcast a fused ground truth w.r.t world.
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 2 March 2019
 */

#pragma once

#include <iostream>
#include <string>
#include <memory>
#include <functional>

#include <ros/ros.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <tf2_ros/transform_listener.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>

#include <acl_msgs/ViconState.h>

#include <std_srvs/Trigger.h>

#include <mocap_utils/utils.h>
#include "mocap_utils/tf2_helpers.h"

namespace acl {
namespace mocap_utils {

  class RigidBody
  {
  public:
    RigidBody(const ros::NodeHandle nh,
              double loopPeriod, double alpha,
              const std::string& name);
    ~RigidBody() = default;
    
    void enableVicon(const std::string& topic);
    void enableOptitrack(const std::string& topic);

  private:
    ros::NodeHandle nh_;
    ros::ServiceServer srv_calibrate_; ///< calibrating dot offset btwn systems
    ros::Subscriber sub_vicon_, sub_optitrack_;
    ros::Publisher pub_pose_, pub_err_, pub_status_;
    ros::Publisher pub_bvicon_, pub_boptitrack_, pub_ifacebias_, pub_toff_;
    ros::Timer tim_publish_;

    // indication of the coverage region
    enum class Region : int { VICON, FORCED_VICON, FUSED, FORCED_OPTITRACK, OPTITRACK };
    Region lastRegion_; ///< coverage region of last timestep

    tf2::Transform ifaceBias_; ///< Interface bias from crossing coverage iface

    bool captureOffset_ = false; ///< should transforms be captured for
                                  ///< calibrating rigid body offset?

    double loopPeriod_; ///< Period of fusion loop.
                        ///< Should be ~1/2 of slowest mocap stream.
    
    double alpha_; ///< filter constant for LPF, 0 ≤ α ≤ 1
    geometry_msgs::PoseStamped filtPose_; ///< LPF'd output pose

    geometry_msgs::TransformStamped msg_T_WV_; ///< vicon w.r.t world (static)
    geometry_msgs::TransformStamped msg_T_WO_; ///< optitrack w.r.t world (static)

    tf2::Transform T_VDB_; ///< body w.r.t vicon dots
    tf2::Transform T_ODB_; ///< body w.r.t optitrack dots
    tf2::Transform T_ODVD_; ///< vicon dots w.r.t optitrack dots (T_ODB * T_VDB^-1)

    std::string name_; ///< robot name
    std::string viconTopic_, optitrackTopic_; ///< topic enabled if not empty

    acl_msgs::ViconStatePtr lastViconMsg_;
    geometry_msgs::PoseStampedPtr lastOptitrackMsg_;

    // flags are set to true when new data is received.
    bool hasNewViconData_ = false;
    bool hasNewOptitrackData_ = false;

    bool hasSeenVicon_ = false;     ///< has body ever been seen by VICON?
    bool hasSeenOptitrack_ = false; ///< has body ever been seen by OptiTrack?

    void viconCb(const acl_msgs::ViconStatePtr& msg);
    void optitrackCb(const geometry_msgs::PoseStampedPtr& msg);
    void timerCb(const ros::TimerEvent& e);

    /**
     * @brief      Ensures that the pose is smooth, esp. across boundaries.
     *
     * @param[in]  lastPose    The pose from the last timestep.
     * @param[in]  newPose     The incoming pose to publish.
     * @param[in]  lastRegion  The coverage region of the last timestep.
     * @param[in]  newRegion   The current coverage region.
     *
     * @return     The smooth pose.
     */
    geometry_msgs::Pose calcSmoothPose(const geometry_msgs::Pose& lastPose,
                                       const geometry_msgs::Pose& newPose,
                                       const Region lastRegion,
                                       const Region newRegion,
                                       bool& shouldSkip);

    /**
     * @brief      Gets the {VICON rigid body w.r.t world} poses as seen by
     *             the two systems (NOTE: dots == rigid body != IMU body frame).
     *
     * @param      vicon_WVD      {VICON dots w.r.t world} as seen by VICON
     * @param      optitrack_WVD  {VICON dots w.r.t world} as seen by OptiTrack
     */
    void getRigidBodyPoses(geometry_msgs::Pose& vicon_WVD,
                           geometry_msgs::Pose& optitrack_WVD) const;

    /**
     * @brief      Low-pass filter on pose (alpha filter)
     * 
     *                  x = alpha_*x + (1-alpha_)*y;
     *
     * @param[in]  alpha Filter parameter
     * @param[in]  y     New pose measurement
     * @param      x     Filtered pose
     */
    void poseLPF(double alpha, const geometry_msgs::Pose& y,
                 geometry_msgs::Pose& x) const;

    /**
     * @brief      Fuse two poses by linearly interpolating (i.e., averaging)
     *
     * @param[in]  a     Pose 1
     * @param[in]  b     Pose 2
     *
     * @return     Average of the two poses
     */
    geometry_msgs::Pose fusePose(const geometry_msgs::Pose& a,
                                 const geometry_msgs::Pose& b) const;

    /**
     * @brief      Calculates the error between two poses
     *
     * @param[in]  a     Pose 1
     * @param[in]  b     Pose 2
     *
     * @return     The relative transformation (i.e., the error)
     */
    geometry_msgs::Pose calcError(const geometry_msgs::Pose& a,
                                  const geometry_msgs::Pose& b) const;

    /**
     * @brief      Checks if pose msg is *exactly* the same
     *
     * @param[in]  a     Pose message
     * @param[in]  b     Pose message
     *
     * @return     true if exactly equal
     */
    bool samePose(const geometry_msgs::Pose& a,
                  const geometry_msgs::Pose& b) const;


    /**
     * @brief      Service callback for calibrating the offset between
     *             VICON and OptiTrack rigid bodies (when covisible).
     *
     * @param      req   The request
     * @param      res   The response
     *
     * @return     True if response handled
     */
    bool calibSrvCb(std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res);

  };

  using RigidBodyPtr = std::shared_ptr<RigidBody>;
}
}
