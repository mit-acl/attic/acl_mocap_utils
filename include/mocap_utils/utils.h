/**
 * @file helpers.h
 * @brief Utility methods associated with mocap fusion
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 5 March 2019
 */
#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <vector>

#include <ros/ros.h>

#include <yaml-cpp/yaml.h>

#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <geometry_msgs/TransformStamped.h>

namespace acl {
namespace mocap_utils {

  class Utils
  {
  public:
    Utils() = delete;
    ~Utils() = delete;
    
    /**
     * @brief      Parse a space-delimitted string into a tf2::Transform
     *
     *
     * @param[in]  xyzYPR  String in format: "x y z Y P R"
     * @param      T       Resulting transform
     *
     * @return     False if invalid format
     */
    static bool parse_xyzYPR(const std::string& xyzYPR, tf2::Transform& T);

    /**
     * @brief      Load a calibrated T_WO transform from yaml
     *
     * @param[in]  file  YAML file to read from
     * @param      T     OptiTrack frame w.r.t world (T_WO)
     *
     * @return     False if unable to open / invalid format
     */
    static bool loadCalibration(const std::string& file,
                                geometry_msgs::TransformStamped& T);

    /**
     * @brief      Save the calibrated T_WO_ transform to yaml
     *
     * @param[in]  file  YAML file to save to
     * @param[in]  T     OptiTrack frame w.r.t world (T_WO)
     */
    static void saveCalibration(const std::string& file,
                                const geometry_msgs::TransformStamped& T);

    /**
     * @brief      Save transform samples to CSV
     *
     * @param[in]  file  CSV file to save to
     * @param[in]  Ts    Vector of transform samples
     */
    static void saveSamples(const std::string& file,
                            const std::vector<tf2::Transform>& Ts);

  };

}
}
