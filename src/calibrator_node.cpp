/**
 * @file calibrator_node.cpp
 * @brief ROS node to calibrate transform between VICON and OptiTrack
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 28 Feb 2019
 */

#include <ros/ros.h>

#include "mocap_utils/calibrator.h"

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "calibrator");
  ros::NodeHandle nh("~");
  acl::mocap_utils::Calibrator obj(nh);
  ros::spin();
  return 0;
}
