/**
 * @file fuser_node.cpp
 * @brief ROS node to fuse VICON and OptiTrack stream for the highbay
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 2 March 2019
 */

#include <ros/ros.h>

#include "mocap_utils/fuser.h"

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "mocap_fuser");
  ros::NodeHandle nh("~");
  acl::mocap_utils::Fuser obj(nh);
  ros::spin();
  return 0;
}
