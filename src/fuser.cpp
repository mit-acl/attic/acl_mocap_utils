/**
 * @file fuser.cpp
 * @brief Fuse VICON and OptiTrack for the highbay
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 2 March 2019
 */

#include "mocap_utils/fuser.h"

namespace acl {
namespace mocap_utils {

Fuser::Fuser(const ros::NodeHandle nh)
  : nh_(nh)
{

  // detect new rigid bodies published by VICON and OptiTrack publishers
  tim_main_ = nh_.createTimer(ros::Duration(1), &Fuser::timerCb, this);

  nh_.param<std::string>("calibration_file", calibFile_, "mocap_calibration.yaml");

  nh_.param("loop_period", loopPeriod_, 0.01);
  nh_.param("lpf_alpha", alpha_, 0.5);

  //
  // Broadcast optitrack frame w.r.t world
  //

  if (!Utils::loadCalibration(calibFile_, T_WO_)) {
    ROS_ERROR_STREAM("Could not load T_WO calibration from " << calibFile_);
    ros::shutdown();
    return;
  }
  staticBr_.sendTransform(T_WO_);
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void Fuser::timerCb(const ros::TimerEvent& e)
{
  // retrieve a list of the current ROS topics
  ros::master::V_TopicInfo topic_infos;
  ros::master::getTopics(topic_infos);

  // Use the topics to find any vicon and optitrack rigid bodies. All that are
  // found will be 'enabled', which is an idempotent action.
  for (const auto& topic : topic_infos) {
    {
      const std::string type = "vicon";
      std::regex r("^/([\\w]+)/" + type + "$");
      std::smatch m;
      if (std::regex_search(topic.name, m, r)) {
        const std::string& name = m[1];
        const std::string& topic = m[0];

        // create a new rigid body if this robot is new
        if (!robots_.count(name)) {
          // TODO: Is this how we want to namespace?
          robots_[name] =
            std::make_shared<RigidBody>(ros::NodeHandle(),
                                        loopPeriod_, alpha_, name);
        }

        // enable/update VICON topic in rigid-body fuser
        robots_[name]->enableVicon(topic);
      }
    }
    {
      const std::string type = "optitrack";
      std::regex r("^/([\\w]+)/" + type + "$");
      std::smatch m;
      if (std::regex_search(topic.name, m, r)) {
        const std::string& name = m[1];
        const std::string& topic = m[0];

        // create a new rigid body if this robot is new
        if (!robots_.count(name)) {
          // TODO: Is this how we want to namespace?
          robots_[name] =
            std::make_shared<RigidBody>(ros::NodeHandle(),
                                        loopPeriod_, alpha_, name);
        }

        // enable/update OptiTrack topic in rigid-body fuser
        robots_[name]->enableOptitrack(topic);
      }
    }
  }
}

} // ns mocap_utils
} // ns acl
