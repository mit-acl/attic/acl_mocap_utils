/**
 * @file utils.cpp
 * @brief Utility methods associated with mocap fusion
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 5 March 2019
 */

#include "mocap_utils/utils.h"

namespace acl {
namespace mocap_utils {

bool Utils::parse_xyzYPR(const std::string& xyzYPR, tf2::Transform& T)
{
  constexpr int expectedValues = 6; // x y z Y P R
  constexpr char delim = ' ';

  std::vector<double> values;
  std::stringstream ss(xyzYPR);

  // attempt to parse the string
  try {
    std::string tmp;
    while (std::getline(ss, tmp, delim)) {
      values.push_back(std::stod(tmp));
    }
  } catch (...) {
    return false;
  }

  if (values.size() != expectedValues) {
    return false;
  }

  // create transform
  tf2::Vector3 p(values[0], values[1], values[2]);
  tf2::Quaternion q;
  q.setRPY(values[5], values[4], values[3]);
  T.setOrigin(p);
  T.setRotation(q);

  return true;
}

// ----------------------------------------------------------------------------

bool Utils::loadCalibration(const std::string& file,
                            geometry_msgs::TransformStamped& T)
{
  YAML::Node node;
  try {
    node = YAML::LoadFile(file);
  } catch (...) {
    return false;
  }

  const auto& trans = node["translation"];
  const auto& quat = node["quaternion"];

  if (!trans || !trans["x"] || !trans["y"] || !trans["z"]) {
    return false;
  }

  if (!quat || !quat["x"] || !quat["y"] || !quat["z"] || !quat["w"]) {
    return false;
  }

  T.header.stamp = ros::Time::now();
  T.header.frame_id = "world";
  T.child_frame_id = "optitrack";
  T.transform.translation.x = trans["x"].as<double>();
  T.transform.translation.y = trans["y"].as<double>();
  T.transform.translation.z = trans["z"].as<double>();
  T.transform.rotation.x = quat["x"].as<double>();
  T.transform.rotation.y = quat["y"].as<double>();
  T.transform.rotation.z = quat["z"].as<double>();
  T.transform.rotation.w = quat["w"].as<double>();

  ROS_INFO_STREAM("Loaded T_WO calibration from " << file);
  std::cout << "translation:" << std::endl;
  std::cout << "  x: " << trans["x"] << std::endl;
  std::cout << "  y: " << trans["y"] << std::endl;
  std::cout << "  z: " << trans["z"] << std::endl;
  std::cout << "quaternion:" << std::endl;
  std::cout << "  x: " << quat["x"] << std::endl;
  std::cout << "  y: " << quat["y"] << std::endl;
  std::cout << "  z: " << quat["z"] << std::endl;
  std::cout << "  w: " << quat["w"] << std::endl;

  return true;
}

// ----------------------------------------------------------------------------

void Utils::saveCalibration(const std::string& file,
                            const geometry_msgs::TransformStamped& T)
{
  YAML::Emitter out;
  out << YAML::BeginMap;

  out << YAML::Key << "translation";
  out << YAML::Value << YAML::BeginMap;
  out << YAML::Key << "x";
  out << YAML::Value << T.transform.translation.x;
  out << YAML::Key << "y";
  out << YAML::Value << T.transform.translation.y;
  out << YAML::Key << "z";
  out << YAML::Value << T.transform.translation.z;
  out << YAML::EndMap;

  out << YAML::Key << "quaternion";
  out << YAML::Value << YAML::BeginMap;
  out << YAML::Key << "x";
  out << YAML::Value << T.transform.rotation.x;
  out << YAML::Key << "y";
  out << YAML::Value << T.transform.rotation.y;
  out << YAML::Key << "z";
  out << YAML::Value << T.transform.rotation.z;
  out << YAML::Key << "w";
  out << YAML::Value << T.transform.rotation.w;
  out << YAML::EndMap;

  out << YAML::EndMap;

  std::cout << out.c_str() << std::endl;

  std::ofstream fout(file);
  fout << out.c_str();

  ROS_INFO_STREAM("Saved T_WO calibration to " << file);
}

// ----------------------------------------------------------------------------

void Utils::saveSamples(const std::string& file, const std::vector<tf2::Transform>& Ts)
{
  std::ofstream fout(file);

  for (const auto& T : Ts) {
    tf2::Vector3 p = T.getOrigin();
    tf2::Quaternion q = T.getRotation();
    fout << p.x() << "," << p.y() << "," << p.z() << ","
         << q.getW() << "," << q.getX() << "," << q.getY() << "," << q.getZ()
         << std::endl;
  }

  fout.close();
}

} // ns mocap_utils
} // ns acl