/**
 * @file rigid_body.cpp
 * @brief A rigid body object can subscribe to VICON and/or OptiTrack
 *        streams and rebroadcast a fused ground truth w.r.t world.
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 2 March 2019
 */

#include "mocap_utils/rigid_body.h"

namespace acl {
namespace mocap_utils {

RigidBody::RigidBody(const ros::NodeHandle nh,
                     double loopPeriod, double alpha,
                     const std::string& name)
  : nh_(nh), loopPeriod_(loopPeriod), alpha_(alpha), name_(name)
{

  pub_bvicon_ = nh_.advertise<geometry_msgs::PoseStamped>(name + "/body_vicon", 1);
  pub_boptitrack_ = nh_.advertise<geometry_msgs::PoseStamped>(name + "/body_optitrack", 1);
  pub_ifacebias_ = nh_.advertise<geometry_msgs::PoseStamped>(name + "/bias", 1);
  pub_toff_ = nh_.advertise<std_msgs::Float32>(name + "/toff", 1);

  // publish rigid body pose w.r.t world
  pub_pose_ = nh_.advertise<geometry_msgs::PoseStamped>(name + "/world", 1);

  // publish error between the two mocap systems (when visible by both)
  pub_err_ = nh_.advertise<geometry_msgs::PoseStamped>(name + "/error", 1);

  // publish a string to specify which mocap system is in use
  pub_status_ = nh_.advertise<std_msgs::Int32>(name + "/mocap_status", 1);

  // allow the user to calibrate for offset between VICON and OptiTrack rigid body
  srv_calibrate_ = nh_.advertiseService(name + "/calib_offset", &RigidBody::calibSrvCb, this);

  tim_publish_ = nh_.createTimer(ros::Duration(loopPeriod_), &RigidBody::timerCb, this);

  //
  // Store transforms from ROS tf tree
  //

  // connect to tf tree so we can transform data
  tf2_ros::Buffer tfBuffer;
  tf2_ros::TransformListener tfListener(tfBuffer);

  try {
    // transform from vicon to world (pose of vicon w.r.t world)
    msg_T_WV_ = tfBuffer.lookupTransform("world", "vicon",
                                            ros::Time(0), ros::Duration(1));
  } catch (tf2::TransformException& ex) {
    ROS_ERROR_STREAM("Expected tf Transform: vicon w.r.t world.");
    ros::shutdown();
    return;
  }

  try {
    // transform from optitrack to world (pose of optitrack w.r.t world)
    msg_T_WO_ = tfBuffer.lookupTransform("world", "optitrack",
                                            ros::Time(0), ros::Duration(1));
  } catch (tf2::TransformException& ex) {
    ROS_ERROR_STREAM("Expected tf Transform: optitrack w.r.t world.");
    ros::shutdown();
    return;
  }

  //
  // Robot body w.r.t vicon/optitrack rigid body markers (dots)
  //

  std::string body_wrt_vicon_dots, body_wrt_optitrack_dots;
  nh_.param<std::string>(name + "/body_wrt_vicon_dots",
                          body_wrt_vicon_dots, "0 0 0 0 0 0");
  nh_.param<std::string>(name + "/body_wrt_optitrack_dots",
                          body_wrt_optitrack_dots, "0 0 0 0 0 0");

  if (!Utils::parse_xyzYPR(body_wrt_vicon_dots, T_VDB_)) {
    ROS_ERROR_STREAM("rosparam 'body_wrt_vicon_dots' expected "
                      "format 'x y z Y P R' "
                      "but got '" << body_wrt_vicon_dots << "'");
    ros::shutdown();
    return;
  }

  if (!Utils::parse_xyzYPR(body_wrt_optitrack_dots, T_ODB_)) {
    ROS_ERROR_STREAM("rosparam 'body_wrt_optitrack_dots' expected "
                      "format is 'x y z Y P R' "
                      "but got '" << body_wrt_optitrack_dots << "'");
    ros::shutdown();
    return;
  }

  // calculate vicon rigid body w.r.t optitrack rigid body
  T_ODVD_ = T_ODB_ * T_VDB_.inverse();

  //
  // Initialize filtered pose
  //

  filtPose_.header.frame_id = "world";
  filtPose_.pose.position.x = 0.0;
  filtPose_.pose.position.y = 0.0;
  filtPose_.pose.position.z = 0.0;
  filtPose_.pose.orientation.x = 0.0;
  filtPose_.pose.orientation.y = 0.0;
  filtPose_.pose.orientation.z = 0.0;
  filtPose_.pose.orientation.w = 1.0;

  // initialize interface bias
  ifaceBias_.setIdentity();

  lastViconMsg_.reset(new acl_msgs::ViconState);
  lastOptitrackMsg_.reset(new geometry_msgs::PoseStamped);

  // so we don't get nans
  lastViconMsg_->pose.orientation.w = 1;
  lastOptitrackMsg_->pose.orientation.w = 1;

  ROS_INFO_STREAM("[Mocap Fuser] New rigid body: " << name_);
}

// ----------------------------------------------------------------------------

void RigidBody::enableVicon(const std::string& topic)
{
  // If this topic is already enabled (and the same), then bail
  if (topic == viconTopic_) return;

  // set topic (signifies that VICON is enabled)
  viconTopic_ = topic;

  sub_vicon_ = nh_.subscribe(topic, 1, &RigidBody::viconCb, this);

  ROS_WARN_STREAM("[Mocap Fuser] " << name_ << " subscribed to " << topic);
}

// ----------------------------------------------------------------------------

void RigidBody::enableOptitrack(const std::string& topic)
{
  // If this topic is already enabled (and the same), then bail
  if (topic == optitrackTopic_) return;

  // set topic (signifies that OptiTrack is enabled)
  optitrackTopic_ = topic;

  sub_optitrack_ = nh_.subscribe(topic, 1, &RigidBody::optitrackCb, this);

  ROS_WARN_STREAM("[Mocap Fuser] " << name_ << " subscribed to " << topic);
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void RigidBody::viconCb(const acl_msgs::ViconStatePtr& msg)
{
  // Assume that if the message is exactly the same, the message is latched
  // and the camera system cannot actually see the rigid body currently.
  if (lastViconMsg_ && samePose(msg->pose, lastViconMsg_->pose)) return;

  hasNewViconData_ = true;

  lastViconMsg_ = msg;

  hasSeenVicon_ = true;
}

// ----------------------------------------------------------------------------

void RigidBody::optitrackCb(const geometry_msgs::PoseStampedPtr& msg)
{
  // Assume that if the message is exactly the same, the message is latched
  // and the camera system cannot actually see the rigid body currently.
  if (lastOptitrackMsg_ && samePose(msg->pose, lastOptitrackMsg_->pose)) return;

  hasNewOptitrackData_ = true;

  lastOptitrackMsg_ = msg;

  hasSeenOptitrack_ = true;
}

// ----------------------------------------------------------------------------

void RigidBody::timerCb(const ros::TimerEvent& e)
{
  // if there are no new measurements, don't publish anything
  if (!hasNewViconData_ && !hasNewOptitrackData_) return;

  // get {VICON rigid body w.r.t world} poses from the two systems
  geometry_msgs::Pose vicon_WVD, optitrack_WVD;
  getRigidBodyPoses(vicon_WVD, optitrack_WVD);

  // (debug) publish the {rigid body w.r.t world} poses from the two systems
  geometry_msgs::PoseStamped msg;
  msg.header.frame_id = "world";

  if (hasNewViconData_) {
    msg.header.stamp = lastViconMsg_->header.stamp; // ros::Time::now();
    msg.pose = vicon_WVD;
    pub_bvicon_.publish(msg);
  }

  if (hasNewOptitrackData_) {
    msg.header.stamp = lastOptitrackMsg_->header.stamp; // ros::Time::now();
    msg.pose = optitrack_WVD;
    pub_boptitrack_.publish(msg);
  }

  //
  // Fusion and combination logic
  //

  geometry_msgs::Pose pose;

  // determine the new operating region
  Region region;

  constexpr double X_THRESH = 5.0;

  if (hasNewViconData_ && hasNewOptitrackData_) {

    // TODO: If one has low quality, don't use it

    // TODO: Remove this hack
    if (lastViconMsg_->pose.position.x <= X_THRESH) {
      pose = vicon_WVD;
      region = Region::FORCED_VICON;
    } else {
      // pose = fusePose(vicon_WVD, optitrack_WVD);
      // region = Region::FUSED;

      pose = optitrack_WVD;
      region = Region::FORCED_OPTITRACK;
    }

  } else if (hasNewViconData_) {

    // use VICON data only
    pose = vicon_WVD;
    region = Region::VICON;

  } else if (hasNewOptitrackData_) {

    // use OptiTrack data only
    pose = optitrack_WVD;
    region = Region::OPTITRACK;

  }

  // don't even listen to optitrack below X_THRESH
  if (hasSeenVicon_ && lastViconMsg_->pose.position.x <= X_THRESH) {
    pose = vicon_WVD;
    region = Region::FORCED_VICON;
  }

  // // just force optitrack
  // pose = optitrack_WVD;
  // region = Region::FORCED_OPTITRACK;

  //
  // Quat sign flip check
  //

  tf2::Quaternion q, qlast;
  tf2::convert(pose.orientation, q);
  tf2::convert(filtPose_.pose.orientation, qlast);

  if (q.dot(qlast) < 0.0) {
    q = -q;
    tf2::convert(q, pose.orientation);
  }


  // keep the pose smooth across the interface of the two systems
  // (i.e., no position jumps, which cause velocity spikes)
  bool skip = false;
  // pose = calcSmoothPose(filtPose_.pose, pose, lastRegion_, region, skip);

  if (skip) {
    // update state
    hasNewViconData_ = false;
    hasNewOptitrackData_ = false;
    lastRegion_ = region;

    return;
  }

  poseLPF(alpha_, pose, filtPose_.pose);

  //
  // Publishers
  //

  if (hasNewViconData_ && hasNewOptitrackData_) {
    std_msgs::Float32 toff;
    toff.data = (lastViconMsg_->header.stamp - lastOptitrackMsg_->header.stamp).toSec();
    pub_toff_.publish(toff);
  }

  filtPose_.header.stamp = ros::Time::now();
  pub_pose_.publish(filtPose_);

  // Publish status message indicating the operating region
  std_msgs::Int32 status;
  status.data = static_cast<int>(region);
  pub_status_.publish(status);

  // Publish relative mocap error
  if (hasNewViconData_ && hasNewOptitrackData_) {
    geometry_msgs::PoseStamped error;
    error.header.stamp = ros::Time::now();

    // NOTE: This is the error between the two mocap systems
    //       and does not say anything about the fused pose.
    error.pose = calcError(vicon_WVD, optitrack_WVD);
    pub_err_.publish(error);
  }

  //
  // 
  //

  if (captureOffset_) {
    if (hasNewViconData_ && hasNewOptitrackData_) {

      // transform {vicon rigid body w.r.t vicon} to {vicon rigid body w.r.t world}
      geometry_msgs::Pose vicon_WVD; // {vicon dots w.r.t world}
      tf2::doTransform(lastViconMsg_->pose, vicon_WVD, msg_T_WV_);
      tf2::Transform T_WVD;
      tf2::convert(vicon_WVD, T_WVD);

      // transform {optitrack rigid body w.r.t optitrack} to {optitrack rigid body w.r.t world}
      geometry_msgs::Pose optitrack_WOD; // {optitrack dots w.r.t world}
      tf2::doTransform(lastOptitrackMsg_->pose, optitrack_WOD, msg_T_WO_);
      tf2::Transform T_WOD;
      tf2::convert(optitrack_WOD, T_WOD);

      // measure the offset
      T_ODVD_ = T_WOD.inverse() * T_WVD;

      captureOffset_ = false;
    }
  }

  // update state
  hasNewViconData_ = false;
  hasNewOptitrackData_ = false;
  lastRegion_ = region;
}

// ----------------------------------------------------------------------------

geometry_msgs::Pose RigidBody::calcSmoothPose(const geometry_msgs::Pose& lastPose,
                                              const geometry_msgs::Pose& newPose,
                                              const Region lastRegion,
                                              const Region newRegion,
                                              bool& shouldSkip)
{
  geometry_msgs::Pose out;

  tf2::Transform pose, oldPose;
  tf2::convert(newPose, pose);
  tf2::convert(lastPose, oldPose);

  // apply the current interface bias to the incoming pose
  tf2::convert(pose * ifaceBias_, out);

  //
  // Check for boundary crossings
  //

  bool vicon2optitrack = (lastRegion == Region::FORCED_VICON || lastRegion == Region::VICON)
          && (newRegion == Region::FORCED_OPTITRACK || newRegion == Region::OPTITRACK);

  bool optitrack2vicon = (lastRegion == Region::FORCED_OPTITRACK || lastRegion == Region::OPTITRACK)
          && (newRegion == Region::FORCED_VICON || newRegion == Region::VICON);

  if (vicon2optitrack || optitrack2vicon) {

    // accumulate interface bias (only in position)
    ifaceBias_ = pose.inverse() * oldPose;
    // ifaceBias_.setOrigin((oldPose.getOrigin() - pose.getOrigin()));
    ifaceBias_.setRotation(tf2::Quaternion::getIdentity());

    // yaw bias only
    // tf2::Quaternion q = ifaceBias_.getRotation();
    // tf2::Quaternion q2(0.0, 0.0, q.getAxis().z(), q.getW());
    // q2.normalize();
    // ifaceBias_.setRotation(q2);

    geometry_msgs::PoseStamped msg;
    msg.header.stamp = ros::Time::now();
    tf2::convert(ifaceBias_, msg.pose);
    pub_ifacebias_.publish(msg);

    // skip publishing this pose (otherwise the position would be exactly the same)
    shouldSkip = true;
  }

  return out;
}

// ----------------------------------------------------------------------------

void RigidBody::getRigidBodyPoses(geometry_msgs::Pose& vicon_WVD,
                                  geometry_msgs::Pose& optitrack_WVD) const
{
  // NOTE: dots == rigid body != IMU body frame

  //
  // VICON Rigid Body w.r.t World according to VICON
  //

  // transform {vicon rigid body w.r.t vicon} to {vicon rigid body w.r.t world}
  tf2::doTransform(lastViconMsg_->pose, vicon_WVD, msg_T_WV_);

  //
  // VICON Rigid Body w.r.t World according to OptiTrack
  //

  geometry_msgs::Pose optitrack_WOD; // {optitrack dots w.r.t world}

  // transform {optitrack rigid body w.r.t optitrack} to {optitrack rigid body w.r.t world}
  tf2::doTransform(lastOptitrackMsg_->pose, optitrack_WOD, msg_T_WO_);

  // transform {optitrack rigid body w.r.t world} to {vicon rigid body w.r.t world}
  tf2::Transform T_WOD; // (optitrack dots)
  tf2::convert(optitrack_WOD, T_WOD);
  tf2::Transform T_WVD = T_WOD * T_ODVD_;
  tf2::convert(T_WVD, optitrack_WVD);
}

// ----------------------------------------------------------------------------

void RigidBody::poseLPF(double alpha, const geometry_msgs::Pose& y,
                        geometry_msgs::Pose& x) const
{

  //
  // Position LPF
  //

  tf2::Vector3 x_pos, y_pos;
  tf2::convert(x.position, x_pos);
  tf2::convert(y.position, y_pos);

  x_pos = alpha*x_pos + (1-alpha)*y_pos;

  tf2::toMsg(x_pos, x.position);

  //
  // Attitude LPF
  //

  tf2::Quaternion x_quat, y_quat;
  tf2::convert(x.orientation, x_quat);
  tf2::convert(y.orientation, y_quat);

  x_quat = x_quat.slerp(y_quat, 1-alpha);

  tf2::convert(x_quat, x.orientation);
}

// ----------------------------------------------------------------------------

geometry_msgs::Pose RigidBody::fusePose(const geometry_msgs::Pose& a,
                                        const geometry_msgs::Pose& b) const
{
  // we can linearly interpolate between two poses using the LPF routine
  // with alpha == 0.5 (i.e., take the average of the two poses)
  constexpr double alpha = 0.5;

  // for const-correctness
  geometry_msgs::Pose avg = b;
  poseLPF(alpha, a, avg);
  return avg;
}

// ----------------------------------------------------------------------------

geometry_msgs::Pose RigidBody::calcError(const geometry_msgs::Pose& a,
                                         const geometry_msgs::Pose& b) const
{
  tf2::Transform T1, T2;
  tf2::convert(a, T1);
  tf2::convert(b, T2);

  // calculate relative transform
  auto Terr = T2 * T1.inverse();

  geometry_msgs::Pose err;
  tf2::convert(Terr, err);

  return err;
}

// ----------------------------------------------------------------------------

bool RigidBody::samePose(const geometry_msgs::Pose& a,
                         const geometry_msgs::Pose& b) const
{
  // we are really checking if the message data is *exactly* the same,
  // which implies that no new data was received, which implies that
  // the rigid body is potentially occluded.
  return a.position.x == b.position.x
      && a.position.y == b.position.y
      && a.position.z == b.position.z
      && a.orientation.x == b.orientation.x
      && a.orientation.y == b.orientation.y
      && a.orientation.z == b.orientation.z
      && a.orientation.w == b.orientation.w;
}

// ----------------------------------------------------------------------------

bool RigidBody::calibSrvCb(std_srvs::Trigger::Request& req,
                           std_srvs::Trigger::Response& res)
{
  captureOffset_ = true;
}

} // ns mocap_utils
} // ns acl
