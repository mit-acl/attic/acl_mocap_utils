/**
 * @file calibrator.cpp
 * @brief For calibrating transform between VICON and OptiTrack
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 28 Feb 2019
 */

#include "mocap_utils/calibrator.h"

namespace acl {
namespace mocap_utils {

Calibrator::Calibrator(const ros::NodeHandle nh)
  : nh_(nh)
{
  sub_vicon_ = nh_.subscribe("vicon", 1, &Calibrator::viconCb, this);
  sub_optitrack_ = nh_.subscribe("optitrack", 1, &Calibrator::optitrackCb, this);

  pub_vicon_body_ = nh_.advertise<geometry_msgs::PoseStamped>("vicon_body", 1);
  pub_optitrack_body_ = nh_.advertise<geometry_msgs::PoseStamped>("optitrack_body", 1);
  pub_err_ = nh_.advertise<geometry_msgs::Pose>("error", 1);

  tim_tf_broadcast_ = nh_.createTimer(ros::Duration(0.01), &Calibrator::timerCb, this);

  srv_sample_ = nh_.advertiseService("sample", &Calibrator::sampleSrvCb, this);

  nh_.param("nrSamples", nrSamples_, 10);
  nh_.param<std::string>("calibration_file", calibFile_, "mocap_calibration.yaml");

  //
  // Initialize transforms w.r.t world
  //

  // vicon frame w.r.t world
  T_WV_.header.frame_id = "world";
  T_WV_.child_frame_id = "vicon";
  T_WV_.transform.rotation.x = 0.0;
  T_WV_.transform.rotation.y = 0.0;
  T_WV_.transform.rotation.z = 0.0;
  T_WV_.transform.rotation.w = 1.0;
  T_WV_.transform.translation.x = 0.0;
  T_WV_.transform.translation.y = 0.0;
  T_WV_.transform.translation.z = 0.0;

  // optitrack frame w.r.t world
  T_WO_.header.frame_id = "world";
  T_WO_.child_frame_id = "optitrack";
  T_WO_.transform.rotation.x = 0.0;
  T_WO_.transform.rotation.y = 0.0;
  T_WO_.transform.rotation.z = 0.0;
  T_WO_.transform.rotation.w = 1.0;
  T_WO_.transform.translation.x = 0.0;
  T_WO_.transform.translation.y = 0.0;
  T_WO_.transform.translation.z = 0.0;

  if (!Utils::loadCalibration(calibFile_, T_WO_)) {
    ROS_INFO_STREAM("Could not find calibration file " << calibFile_);
  }

  //
  // Robot body w.r.t vicon/optitrack rigid body markers (dots)
  //

  std::string body_wrt_vicon_dots, body_wrt_optitrack_dots;
  nh_.param<std::string>("body_wrt_vicon_dots",
                          body_wrt_vicon_dots, "0 0 0 0 0 0");
  nh_.param<std::string>("body_wrt_optitrack_dots",
                          body_wrt_optitrack_dots, "0 0 0 0 0 0");

  if (!Utils::parse_xyzYPR(body_wrt_vicon_dots, T_VDB_)) {
    ROS_ERROR_STREAM("rosparam 'body_wrt_vicon_dots' expected "
                      "format 'x y z Y P R' "
                      "but got '" << body_wrt_vicon_dots << "'");
    ros::shutdown();
    return;
  }

  if (!Utils::parse_xyzYPR(body_wrt_optitrack_dots, T_ODB_)) {
    ROS_ERROR_STREAM("rosparam 'body_wrt_optitrack_dots' expected "
                      "format is 'x y z Y P R' "
                      "but got '" << body_wrt_optitrack_dots << "'");
    ros::shutdown();
    return;
  }

}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void Calibrator::viconCb(const acl_msgs::ViconStatePtr& msg)
{
  // vicon dots frame w.r.t vicon frame
  tf2::Transform T_VVD;
  tf2::convert(msg->pose, T_VVD);
  
  // re-publish body w.r.t vicon for rviz
  geometry_msgs::PoseStamped pose;
  pose.header.stamp = ros::Time::now();
  pose.header.frame_id = "vicon";

  // compose with {body w.r.t vicon dots}
  auto T_VB = T_VVD * T_VDB_;
  tf2::convert(T_VB, pose.pose);

  pub_vicon_body_.publish(pose);

  // save latest for error calc
  msg_T_VB_ = pose;

  if (capture_) {
    buf_T_VVD_.push_back(T_VVD);
  }
}

// ----------------------------------------------------------------------------

void Calibrator::optitrackCb(const acl_msgs::ViconStatePtr& msg)
{
  // optitrack dots w.r.t optitrack frame
  tf2::Transform T_OOD;
  tf2::convert(msg->pose, T_OOD);

  // re-publish for rviz
  geometry_msgs::PoseStamped pose;
  pose.header.stamp = ros::Time::now();
  pose.header.frame_id = "optitrack";
  
  // compose with {body w.r.t optitrack dots}
  auto T_OB = T_OOD * T_ODB_;
  tf2::convert(T_OB, pose.pose);

  pub_optitrack_body_.publish(pose);

  // save latest for error calc
  msg_T_OB_ = pose;

  if (capture_) {
    buf_T_OOD_.push_back(T_OOD);
  }
}

// ----------------------------------------------------------------------------

void Calibrator::timerCb(const ros::TimerEvent& e)
{
  // update timestamps
  T_WV_.header.stamp = ros::Time::now();
  T_WO_.header.stamp = ros::Time::now();

  tf_br_.sendTransform(T_WV_);
  tf_br_.sendTransform(T_WO_);

  // calculate calibration pose error
  if ((msg_T_VB_.header.stamp - msg_T_OB_.header.stamp) < ros::Duration(0.2)) {
    tf2::Transform T_VB, T_OB, T_WV, T_WO;
    tf2::convert(msg_T_VB_.pose, T_VB);
    tf2::convert(msg_T_OB_.pose, T_OB);
    tf2::convert(T_WV_.transform, T_WV);
    tf2::convert(T_WO_.transform, T_WO);

    // mocap body w.r.t world---we would like these to be identity
    auto T_WVB = T_WV * T_VB;
    auto T_WOB = T_WO * T_OB;

    tf2::Transform T_error = T_WVB * T_WOB.inverse();

    geometry_msgs::Pose err;
    tf2::convert(T_error, err);

    pub_err_.publish(err);
  }

  // check if we are done capturing
  if (capture_) {
    // How many samples (total) should we expect
    // in the buffer after this capture?
    const int totalSamples = nrCaptures_*nrSamples_;

    if (buf_T_OOD_.size() >= totalSamples && buf_T_VVD_.size() >= totalSamples) {
      capture_ = false;
      ROS_INFO_STREAM("[Capture " << nrCaptures_ << "] Finished.");

      // make sure that we have the same number of transforms
      buf_T_VVD_.resize(totalSamples);
      buf_T_OOD_.resize(totalSamples);

      // find the relative transformation between vicon and optitrack
      calibrate();

      // save the samples to file
      Utils::saveSamples("vicondots_wrt_vicon.csv", buf_T_VVD_);
      Utils::saveSamples("optitrackdots_wrt_optitrack.csv", buf_T_OOD_);
    }
  }
}

// ----------------------------------------------------------------------------

void Calibrator::calibrate()
{
  ROS_WARN_STREAM("Beginning calibration.");

  // How many total transform samples should we have?
  const size_t totalSamples = nrCaptures_*nrSamples_;
  assert(buf_T_VVD_.size() == totalSamples);
  assert(buf_T_OOD_.size() == totalSamples);

  //
  // Cumulative moving average filter
  //

  // initialize the average pos, quat
  tf2::Vector3 p_VO;
  tf2::Quaternion q_VO = tf2::Quaternion::getIdentity();

  for (size_t n=0; n<totalSamples; ++n) {
    // measurements of {vicon to vicon dots} and {optitrack to optitrack dots}
    auto Tbar_VVD = buf_T_VVD_[n];
    auto Tbar_OOD = buf_T_OOD_[n];

    // new measurement: optitrack frame w.r.t vicon frame
    auto Tbar_VO = (Tbar_VVD*T_VDB_) * (Tbar_OOD*T_ODB_).inverse();

    // break out rotation (quat) and translation measurement updates
    auto qbar_VO = Tbar_VO.getRotation();
    auto pbar_VO = Tbar_VO.getOrigin();

    // cumulative moving average
    p_VO = 1.0/(n+1) * (n*p_VO + pbar_VO);

    // TODO: Compare against min||R-Rbar||_F

    // SLERP for quats
    q_VO = q_VO.slerp(qbar_VO, 1.0/(n+1));
  }

  // pack as transform
  tf2::Transform T_VO;
  T_VO.setOrigin(p_VO);
  T_VO.setRotation(q_VO);

  // vicon frame w.r.t world frame (known by design)
  tf2::Transform T_WV;
  tf2::convert(T_WV_.transform, T_WV);

  // optitrack frame w.r.t world frame (what we are calibrating for)
  tf2::Transform T_WO = T_WV * T_VO;
  tf2::convert(T_WO, T_WO_.transform);

  ROS_WARN_STREAM("Calibration complete.");

  Utils::saveCalibration(calibFile_, T_WO_);
}

// ----------------------------------------------------------------------------

bool Calibrator::sampleSrvCb(std_srvs::Trigger::Request& req,
                             std_srvs::Trigger::Response& res)
{
  // A capture represents a set of samples that were retrieved, typically from
  // different locations. This helps average out the noise a little more.
  nrCaptures_++;

  ROS_INFO_STREAM("[Capture " << nrCaptures_ << "] "
                  "Capturing " << nrSamples_ << " transform samples.");

  // begin capturing
  capture_ = true;

  res.success = true;
  return true;
}

} // ns mocap_utils
} // ns acl
